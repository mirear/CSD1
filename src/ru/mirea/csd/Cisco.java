package ru.mirea.csd;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Cisco {

    private static AtomicInteger age = new AtomicInteger(36);

    public static void call(){
        System.out.println("AGE: " + age.get());
        inc(age);
        System.out.println("AGE: " + age.get());

        Box box = new Box("A");
        System.out.println("BOX: " + box.getName());
        change(box);
        System.out.println("BOX: " + box.getName());
    }

    private static void change(Box box) {
        box.setName(null);
        box = null;
    }


    public static void inc(AtomicInteger age_) {
        age_.addAndGet(1);
        age_ = null;
    }

    public static void inc(int age_) {
        age_ = age_ + 1;
    }




}
