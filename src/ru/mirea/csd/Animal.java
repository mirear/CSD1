package ru.mirea.csd;

public interface Animal {

    void say();

    @TimeLogger
    void alarm();

}
