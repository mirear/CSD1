package ru.mirea.csd;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Animal animal = new Cat(1,2);

        Class[] classes = {Animal.class};

        Object animalProxy = Proxy.newProxyInstance(
                Main.class.getClassLoader(),
                classes,
                (o, method, objects) -> {
                    if (method.getAnnotation(TimeLogger.class) != null) {
                        long start = System.currentTimeMillis();
                        method.invoke(animal, objects);
                        long end = System.currentTimeMillis();
                        long result = end - start;
                        System.out.println("Total time of " + method.getName() + " invocation is: " + result);
                    } else {
                        method.invoke(animal, objects);
                    }

                    return null;
                }
        );

        Animal catProxy = ((Animal)animalProxy);

        catProxy.say();
        catProxy.alarm();

    }

    private static void doSomething(Cat cat) {
        cat.setAge(100);
    }

}
