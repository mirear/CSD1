package ru.mirea.csd;

import java.util.HashMap;
import java.util.Map;

public class HashMap_Test {

    public static void main(String[] args) {
        Box box = new Box("A");
        Map<Box, String> boxes = new HashMap<>();
        boxes.put(box, "100kg");
        System.out.println("Weight: " + boxes.get(box));
        change(box);
        System.out.println("Weight: " + boxes.get(box));
    }

    private static void change(Box box) {
        //..
    }

}
